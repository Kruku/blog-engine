package com.example.projekt.controllers;

import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.dto.UpdateUserDto;
import com.example.projekt.dto.UserDto;
import com.example.projekt.dto.UserLoginDto;
import com.example.projekt.model.User;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/users")
    public ModelAndView editUserAuthorities(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView modelAndView = new ModelAndView("users");
            List<UserDto> users = userService.getAllUsers();
            modelAndView.addObject("users", users);
            return modelAndView;
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @GetMapping("/registration")
    public ModelAndView createUser(Model model) {
        ModelAndView mav = new ModelAndView("registration");
        model.addAttribute("user", new CreateUserDto());
        return mav;
    }

    @PostMapping("/registration")
    public ModelAndView createUser(@Valid @ModelAttribute(name = "user") CreateUserDto user,
                                   BindingResult bindingResult, HttpServletResponse httpServletResponse) {
        ModelAndView mav = new ModelAndView("registration");
        if (bindingResult.hasErrors()) {
            mav.setStatus(HttpStatus.BAD_REQUEST);
            return mav;
        }
        try {
            userService.findUserByLogin(user.getLogin());
            String userWithSameLoginExists = "Login already taken";
            mav.addObject("userWithSameLoginExists", userWithSameLoginExists);
            httpServletResponse.setStatus(409);
            return mav;
        } catch (UserNotFoundException e) {
            if (!user.getPassword().equals(user.getRepeatedPassword())) {
                String wrongRepeatedPassword = "Passwords have to match";
                mav.addObject("wrongRepeatedPassword", wrongRepeatedPassword);
                httpServletResponse.setStatus(400);
                return mav;
            }
        }
        mav.setViewName("handlers");
        mav.addObject("userCreated", true);
        userService.createUser(user);
        httpServletResponse.setStatus(201);
        return mav;
    }

    @GetMapping("/login")
    public ModelAndView login(Model model) {
        ModelAndView modelAndView = new ModelAndView("login");
        UserLoginDto userLoginDto = new UserLoginDto();
        model.addAttribute("userData", userLoginDto);
        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView loginHandler(@ModelAttribute UserLoginDto userLoginDto, HttpSession httpSession,
                                     HttpServletResponse response) {
        ModelAndView mav = new ModelAndView();
        String badRequest;
        User userByLogin = null;
        try {
            userByLogin = userService.findUserByLogin(userLoginDto.getLogin());
            if (!passwordEncoder.matches(userLoginDto.getPassword(), userByLogin.getPassword())) {
                badRequest = "Password do not match to provided login";
                mav.setViewName("login");
                mav.addObject("badRequest", badRequest);
                response.setStatus(400);
                return mav;
            }
        } catch (UserNotFoundException e) {
            badRequest = "User with provided login do not exist";
            mav.setViewName("login");
            mav.addObject("badRequest", badRequest);
            response.setStatus(404);
            return mav;
        }
        httpSession.setAttribute("userLogin", userByLogin.getLogin());
        boolean isAdmin = userByLogin.getRole().equals("ADMIN");
        httpSession.setAttribute("userRole", isAdmin);
        mav.setViewName("login_handler");
        return mav;
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpSession httpSession) {
        httpSession.invalidate();
        return new ModelAndView("redirect:/index");
    }

    @GetMapping("/users/edit/{login}")
    public ModelAndView setUserRole(@PathVariable String login, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            User userByLogin = null;
            ModelAndView mav = new ModelAndView();
            try {
                userByLogin = userService.findUserByLogin(login);
                UpdateUserDto updateUserDto = new UpdateUserDto().builder()
                        .login(userByLogin.getLogin())
                        .role(userByLogin.getRole())
                        .build();
                mav.setViewName("set_user_role");
                mav.addObject("user", updateUserDto);
                return mav;
            } catch (UserNotFoundException e) {
                mav.setViewName("handlers");
                mav.addObject("userNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @PutMapping(value = "/users/edit/{login}")
    public ModelAndView setUserRole(@Valid @ModelAttribute(name = "user") UpdateUserDto dto, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse, BindingResult bindingResult) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("handlers");
            if (bindingResult.hasErrors()) {
                mav.addObject("badRequest", true);
                mav.setStatus(HttpStatus.BAD_REQUEST);
                return mav;
            } else if (!dto.getRole().equalsIgnoreCase("ADMIN") && !dto.getRole().equalsIgnoreCase("USER")) {
                mav.addObject("badRequest", true);
                httpServletResponse.setStatus(400);
                return mav;
            } else
                try {
                    userService.setUserRole(dto);
                    mav.addObject("userUpdated", true);
                    httpServletResponse.setStatus(201);
                    return mav;
                } catch (UserNotFoundException e) {
                    mav.addObject("userNotFound", true);
                    httpServletResponse.setStatus(404);
                    return mav;
                }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @DeleteMapping("/users/edit/{login}")
    public ModelAndView deleteUser(@PathVariable String login, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("handlers");
            try {
                userService.deleteUser(login);
                httpServletResponse.setStatus(200);
                mav.addObject("userDeleted", true);
                mav.addObject("login", login);
                return mav;
            } catch (UserNotFoundException e) {
                mav.addObject("userNotFound", true);
                httpServletResponse.setStatus(400);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }
}
