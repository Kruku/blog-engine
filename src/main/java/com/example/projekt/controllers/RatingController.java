package com.example.projekt.controllers;

import com.example.projekt.services.EntryService;
import com.example.projekt.services.RatingService;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class RatingController {
    @Autowired
    private RatingService ratingService;
    @Autowired
    private EntryService entryService;

    @GetMapping("/entry/{entryId}/{rating}")
    public ModelAndView entryRating(@PathVariable String entryId, @PathVariable String rating,
                                    HttpSession httpSession, HttpServletResponse httpServletResponse) {
        ModelAndView mav = new ModelAndView("handlers");
        if (Integer.parseInt(rating) > 5 || Integer.parseInt(rating) < 0) {
            httpServletResponse.setStatus(400);
            mav.addObject("ratingBadRequest", true);
            return mav;
        }
        if (httpSession.getAttribute("userLogin") != null) {
            try {
                entryService.findEntryById(Integer.parseInt(entryId));
                String userLogin = (String) httpSession.getAttribute("userLogin");
                ratingService.rateEntry(entryId, rating, userLogin);
                mav.addObject("correctlyVoted", true);
                return mav;
            } catch (EntryNotFoundException e) {
                mav.addObject("entryNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        mav.setViewName("login");
        httpServletResponse.setStatus(401);
        return mav;
    }
}
