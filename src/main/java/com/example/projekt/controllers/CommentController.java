package com.example.projekt.controllers;

import com.example.projekt.dto.CommentDto;
import com.example.projekt.dto.CreateCommentDto;
import com.example.projekt.services.CommentService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.CommentNotFoundException;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;

    @PostMapping("/comment/create")
    public ModelAndView createComment(@Valid @ModelAttribute(name = "comment") CreateCommentDto createCommentDto,
                                      HttpSession httpSession, HttpServletResponse httpServletResponse,
                                      BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView("handlers");
        if (bindingResult.hasErrors() || createCommentDto.getComment() == null || createCommentDto.getComment().length() < 1) {
            mav.addObject("commentValidationError", true);
            mav.setStatus(HttpStatus.BAD_REQUEST);
            return mav;
        }
        if (httpSession.getAttribute("userLogin") != null) {
            try {
                commentService.createComment((String) httpSession.getAttribute("userLogin"), createCommentDto.getComment(), createCommentDto.getEntryId());
                mav.addObject("commentCreated", true);
                httpServletResponse.setStatus(201);
                return mav;
            } catch (EntryNotFoundException e) {
                mav.addObject("entryDoesNotExist", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        mav.setViewName("login");
        httpServletResponse.setStatus(401);
        return mav;
    }

    @GetMapping("/comments")
    public ModelAndView deleteComment(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("comments");
            List<CommentDto> allComments = commentService.getAllComments();
            mav.addObject("comments", allComments);
            return mav;
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @DeleteMapping("/comments/delete/{id}")
    public ModelAndView deleteComment(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("handlers");
            try {
                commentService.deleteCommentById(Integer.parseInt(id));
                mav.addObject("commentDeleted", true);
                httpServletResponse.setStatus(201);
                return mav;
            } catch (CommentNotFoundException e) {
                mav.addObject("commentNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

}
