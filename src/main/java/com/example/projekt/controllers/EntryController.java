package com.example.projekt.controllers;

import com.example.projekt.dto.*;
import com.example.projekt.model.Entry;
import com.example.projekt.services.CommentService;
import com.example.projekt.services.EntryService;
import com.example.projekt.services.RatingService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
public class EntryController {
    @Autowired
    private EntryService entryService;
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private RatingService ratingService;

    @RequestMapping("")
    public ModelAndView projectInformation(){
        ModelAndView mav = new ModelAndView("about");
        return mav;
    }

    @GetMapping("/index")
    public ModelAndView getAllEntries(Model model) {
        ModelAndView modelAndView = new ModelAndView("index");
        List<EntryDto> entryDtos = entryService.getAllEntries();
        List<CommentDto> commentDtos = commentService.getAllComments();
        List<AverageEntryRatingDto> ratingDtos = ratingService.averageRating();
        modelAndView.addObject("entries", entryDtos);
        modelAndView.addObject("comments", commentDtos);
        modelAndView.addObject("ratings", ratingDtos);
        model.addAttribute("comment", new CreateCommentDto());
        model.addAttribute("rate", new String());
        return modelAndView;
    }

    @GetMapping("/entry/create")
    public ModelAndView createEntry(Model model, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            model.addAttribute("entry", new CreateEntryDto());
            return new ModelAndView("create_entry");
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @PostMapping("/entry/create")
    public ModelAndView createEntry(@Valid @ModelAttribute(name = "entry") CreateEntryDto entry,
                                    BindingResult bindingResult, HttpSession httpSession,
                                    HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView();
            if (bindingResult.hasErrors()) {
                mav.setViewName("create_entry");
                mav.setStatus(HttpStatus.BAD_REQUEST);
                return mav;
            }
            String userLogin = (String) httpSession.getAttribute("userLogin");
            CreateEntryDto newEntry = new CreateEntryDto(entry.getEntry(), entry.getTitle());
            entryService.createEntry(newEntry, userLogin);
            mav.setViewName("handlers");
            mav.addObject("entryCreated", true);
            httpServletResponse.setStatus(201);
            return mav;
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @GetMapping("/entry/edit")
    public ModelAndView editEntry(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("edit_entry");
            List<EntryDto> entryDtos = entryService.getAllEntries();
            mav.addObject("entries", entryDtos);
            return mav;
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @GetMapping("/entry/edit/{id}")
    public ModelAndView editEntryById(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("update_entry");
            Integer idAsInt = Integer.parseInt(id);
            Entry entryById = null;
            try {
                entryById = entryService.findEntryById(idAsInt);
                UpdateEntryDto updateEntryDto = new UpdateEntryDto().builder()
                        .title(entryById.getTitle())
                        .entry(entryById.getEntry())
                        .id(entryById.getId())
                        .build();
                mav.setViewName("update_entry");
                mav.addObject("entry", updateEntryDto);
                return mav;
            } catch (EntryNotFoundException e) {
                mav.setViewName("handlers");
                mav.addObject("entryNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @PutMapping("/entry/edit/{id}")
    public ModelAndView editEntryById(@Valid @ModelAttribute(name = "entry") UpdateEntryDto entry, BindingResult bindingResult
            , HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView();
            if (bindingResult.hasErrors()) {
                mav.setViewName("update_entry");
                mav.setStatus(HttpStatus.BAD_REQUEST);
                return mav;
            }
            try {
                entryService.editEntry(entry);
                mav.setViewName("handlers");
                mav.addObject("entryUpdated", true);
                httpServletResponse.setStatus(201);
                return mav;
            } catch (EntryNotFoundException e) {
                mav.setViewName("handlers");
                mav.addObject("entryNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

    @DeleteMapping("/entry/edit/{id}")
    public ModelAndView deleteEntryById(@PathVariable String id, HttpSession httpSession, HttpServletResponse httpServletResponse) {
        if (userService.userRoleValidator(httpSession, httpServletResponse) == null) {
            ModelAndView mav = new ModelAndView("handlers");
            try {
                Integer idAsInt = Integer.parseInt(id);
                entryService.deleteEntryById(idAsInt);
                mav.addObject("entryDeleted", true);
                httpServletResponse.setStatus(201);
                return mav;
            } catch (EntryNotFoundException e) {
                mav.addObject("entryNotFound", true);
                httpServletResponse.setStatus(404);
                return mav;
            }
        }
        return userService.userRoleValidator(httpSession, httpServletResponse);
    }

}
