package com.example.projekt.init;

import com.example.projekt.dto.CreateEntryDto;
import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.services.EntryService;
import com.example.projekt.services.UserService;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import com.example.projekt.services.exceptions.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class InitDataService {
    private final UserService userService;
    private final EntryService entryService;

    @PostConstruct
    public void init() {
        CreateUserDto createUserDto = CreateUserDto.builder()
                .login("Admin")
                .password("admin")
                .repeatedPassword("admin")
                .name("admin")
                .role("admin")
                .build();
        try {
            userService.findUserByLogin(createUserDto.getLogin());
        } catch (UserNotFoundException e) {
            userService.createUser(createUserDto);
        }
        CreateUserDto createUserDto2 = CreateUserDto.builder()
                .login("User")
                .password("user")
                .repeatedPassword("user")
                .name("user")
                .role("user")
                .build();
        try {
            userService.findUserByLogin(createUserDto2.getLogin());
        } catch (UserNotFoundException e) {
            userService.createUser(createUserDto2);
        }
        CreateEntryDto createEntryDto = new CreateEntryDto().builder()
                .entry("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla fermentum at leo vel vestibulum. Nam eu arcu eu justo auctor suscipit a sit amet lorem. Ut eleifend iaculis fermentum. Sed diam justo, dignissim at elit vitae, tincidunt hendrerit turpis. Praesent in gravida quam. Mauris magna libero, porttitor at nunc sed, bibendum semper erat. In eget quam nulla. Vestibulum sagittis libero sollicitudin sollicitudin placerat. Nunc ut congue lectus. Duis vel malesuada leo, eu aliquet sapien. Aenean ac finibus dui.\n" +
                        "\n" +
                        "Vestibulum pharetra dui non mauris placerat eleifend. Sed pharetra ex id tellus feugiat mattis. Aenean in purus id nisl finibus tincidunt eget at ex. Maecenas sollicitudin consectetur diam, eu volutpat nunc ultricies sed. Morbi quis orci semper, tristique orci in, imperdiet lorem. Praesent in dictum urna. Quisque nec consequat risus. Phasellus aliquet volutpat sem vel cursus. Phasellus arcu leo, fermentum id mauris a, fermentum consectetur metus. In lobortis pellentesque enim vel gravida. Curabitur molestie consectetur bibendum.\n" +
                        "\n" +
                        "Curabitur tempor purus in accumsan tincidunt. Nulla consectetur nunc ac lorem convallis cursus. Vivamus a erat sit amet massa tempor volutpat. Phasellus hendrerit ornare pharetra. Ut ut magna hendrerit, mollis turpis eu, ultricies velit. Ut consectetur quam et risus auctor tincidunt. Quisque consectetur dictum est ac hendrerit. Aliquam interdum faucibus risus eu accumsan. Aenean iaculis nulla eu nulla efficitur ultricies. Duis lacinia facilisis nisi quis euismod. Ut sit amet varius velit. Nunc eget tincidunt sem.\n" +
                        "\n" +
                        "Cras ante leo, feugiat ac semper dignissim, facilisis sit amet lorem. Vivamus condimentum, justo in semper lobortis, nisi leo ultrices lorem, nec maximus sapien turpis at nibh. Cras a luctus augue. Fusce eget mollis metus, ac sodales lectus. Donec vestibulum, eros et laoreet tempor, risus tortor dapibus nisl, vitae aliquam quam nunc a nibh. Proin eu justo ex. Cras scelerisque accumsan tellus, a consectetur massa tincidunt at. Morbi ut nisi dictum purus tincidunt finibus. Donec eu pretium nisl. Ut imperdiet felis urna, pellentesque facilisis elit facilisis eget.\n" +
                        "\n" +
                        "Phasellus consequat est est, ac accumsan ipsum dignissim ac. Donec at facilisis mauris. Fusce dignissim sed ex ac sagittis. Ut a dui in arcu aliquam aliquam. Pellentesque a scelerisque purus. Mauris hendrerit efficitur lacinia. In hac habitasse platea dictumst. Maecenas vel velit massa. Nulla odio libero, cursus iaculis vehicula quis, pretium vel leo. Quisque gravida egestas molestie. Nulla dui ante, venenatis id pretium ut, pharetra eget mi. Donec felis lectus, condimentum id hendrerit sed, faucibus ullamcorper ligula. Quisque faucibus malesuada dictum. Curabitur sagittis, nisi id aliquam eleifend, dui mauris porttitor lorem, ac vestibulum lectus orci ac mi.")
                .title("Lorem Ipsum")
                .build();
        try {
            entryService.findEntryByTitle(createEntryDto.getTitle());
        } catch (EntryNotFoundException e) {
            entryService.createEntry(createEntryDto, "Admin");
        }
        CreateEntryDto createEntryDto2 = new CreateEntryDto().builder()
                .entry("Application functionality focusses on possibillity to create blog without using wordpress. Example accounts created during launch of application is: login: Admin, password: admin (ADMIN role) and login: User, password: user (role USER).  ")
                .title("Blog engine project")
                .build();
        try {
            entryService.findEntryByTitle(createEntryDto2.getTitle());
        } catch (EntryNotFoundException e) {
            entryService.createEntry(createEntryDto2, "Admin");
        }
    }
}
