package com.example.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateEntryDto {
    @NotBlank(message = "Entry can not be empty")
    @NotNull(message = "Entry can not be null")
    private String entry;
    @NotBlank(message = "Title is mandatory")
    @NotNull(message = "Title can not be null")
    private String title;


}
