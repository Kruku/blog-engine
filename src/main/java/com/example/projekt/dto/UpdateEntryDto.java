package com.example.projekt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateEntryDto {
    @NotBlank(message = "Title is mandatory")
    @NotNull(message = "Title can not be null")
    private String title;
    @NotBlank(message = "Entry can not be empty")
    @NotNull(message = "Entry can not be null")
    private String entry;
    @NotNull(message = "Id can not be null")
    private Integer id;
}
