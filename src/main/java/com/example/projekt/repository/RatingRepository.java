package com.example.projekt.repository;

import com.example.projekt.model.EntryRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends JpaRepository<EntryRating, Integer> {
}
