package com.example.projekt.services;

import com.example.projekt.dto.CreateUserDto;
import com.example.projekt.dto.UpdateUserDto;
import com.example.projekt.dto.UserDto;
import com.example.projekt.model.User;
import com.example.projekt.repository.UserRepository;
import com.example.projekt.services.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EntryService entryService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDtos = users.stream()
                .map(user -> userMapper.userToDto(user))
                .collect(Collectors.toList());
        return usersDtos;
    }

    public UserDto createUser(CreateUserDto createDto) {
        if (createDto.getRole() == null || !createDto.getRole().equalsIgnoreCase("ADMIN")) {
            createDto.setRole("USER");
        }
        User createdUser = User.builder()
                .login(createDto.getLogin())
                .password(passwordEncoder.encode(createDto.getPassword()))
                .name(createDto.getName())
                .role(createDto.getRole().toUpperCase())
                .build();
        userRepository.save(createdUser);
        return userMapper.userToDto(createdUser);
    }

    public User findUserByLogin(String login) throws UserNotFoundException {
        return userRepository
                .findAll()
                .stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException());
    }

    public UserDto setUserRole(UpdateUserDto dto) throws UserNotFoundException {
        User userByLogin = findUserByLogin(dto.getLogin());
        userByLogin.setRole(dto.getRole().toUpperCase());
        userRepository.save(userByLogin);
        return userMapper.userToDto(userByLogin);
    }

    public UserDto deleteUser(String login) throws UserNotFoundException {
        User userByLogin = findUserByLogin(login);
        entryService.deleteEntriesByUserLogin(login);
        userRepository.delete(userByLogin);
        return userMapper.userToDto(userByLogin);
    }

    public ModelAndView userRoleValidator(HttpSession httpSession, HttpServletResponse httpServletResponse) {
        ModelAndView mav = new ModelAndView();
        User userByLogin = null;
        try {
            userByLogin = findUserByLogin((String) httpSession.getAttribute("userLogin"));
        } catch (UserNotFoundException e) {
            mav.setViewName("login");
            httpServletResponse.setStatus(401);
            return mav;
        }
        if (!userByLogin.getRole().equals("ADMIN")) {
            mav.setViewName("permission_errors");
            httpServletResponse.setStatus(401);
            return mav;
        }
        return null;
    }
}
