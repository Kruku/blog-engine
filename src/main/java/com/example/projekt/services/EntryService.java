package com.example.projekt.services;

import com.example.projekt.dto.*;
import com.example.projekt.model.Entry;
import com.example.projekt.repository.EntryRepository;
import com.example.projekt.services.exceptions.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EntryService {
    @Autowired
    private EntryMapper entryMapper;
    @Autowired
    private EntryRepository entryRepository;
    @Autowired
    private CommentService commentService;

    public EntryDto createEntry(CreateEntryDto e, String author) {
        Entry entry = Entry.builder()
                .author(author)
                .entry(e.getEntry())
                .title(e.getTitle())
                .build();
        entryRepository.save(entry);
        return entryMapper.toDto(entry);
    }

    public List<EntryDto> getAllEntries() {
        List<Entry> entries = entryRepository.findAll();
        List<EntryDto> entryDtos = new ArrayList<>();
        for (int i = entries.size() - 1; i > -1; i--) {
            entryDtos.add(entryMapper.toDto(entries.get(i)));
        }
        return entryDtos;
    }

    public Entry findEntryById(Integer id) throws EntryNotFoundException {
        return entryRepository
                .findAll()
                .stream()
                .filter(entry -> entry.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new EntryNotFoundException());
    }

    public Entry findEntryByTitle(String title) throws EntryNotFoundException {
        return entryRepository
                .findAll()
                .stream()
                .filter(e -> e.getTitle().equals(title))
                .findFirst()
                .orElseThrow(() -> new EntryNotFoundException());
    }

    public EntryDto editEntry(UpdateEntryDto dto) throws EntryNotFoundException {
        Entry entryById = findEntryById(dto.getId());
        if (!dto.getTitle().equals(entryById.getTitle())) {
            entryById.setTitle(dto.getTitle());
        }
        if (!dto.getEntry().equals(entryById.getEntry())) {
            entryById.setEntry(dto.getEntry());
        }
        entryRepository.save(entryById);
        return entryMapper.toDto(entryById);
    }

    public EntryDto deleteEntryById(Integer entryId) throws EntryNotFoundException {
        Entry entryById = findEntryById(entryId);
        commentService.deleteCommentsByEntryId(entryId);
        entryRepository.delete(entryById);
        return entryMapper.toDto(entryById);
    }

    public List<EntryDto> deleteEntriesByUserLogin(String login) {
        List<Entry> entriesByUserLogin = entryRepository
                .findAll()
                .stream()
                .filter(e -> e.getAuthor().equals(login))
                .collect(Collectors.toList());
        List<Integer> entriesId = entriesByUserLogin.stream()
                .map(entry -> Integer.valueOf(entry.getId()))
                .collect(Collectors.toList());
        commentService.deleteCommentsByEntryIds(entriesId);
        entryRepository.deleteAll(entriesByUserLogin);
        return entryMapper.entriesToDto(entriesByUserLogin);
    }

}
