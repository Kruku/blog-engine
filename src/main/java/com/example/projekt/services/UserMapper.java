package com.example.projekt.services;

import com.example.projekt.dto.UserDto;
import com.example.projekt.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserDto userToDto(User u) {
        return UserDto.builder()
                .id(u.getId())
                .login(u.getLogin())
                .name(u.getName())
                .role(u.getRole())
                .build();
    }
}
